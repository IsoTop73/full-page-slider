(function($) {
  $.fn.fullPageSlider = function(_options) {
    var $slider = this;
    var slideIndex = [];
    var slidesAmount = [];
    var $content = $('#slider-content');
    var $container = $('.slides-container');
    var isTouchDevice = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|playbook|silk|BlackBerry|BB10|Windows Phone|Tizen|Bada|webOS|IEMobile|Opera Mini)/);
    var options = $.extend({
      scrollingSpeed: 700,
      slidingSpeed: 1000,
      navigation: true,
      thumbnails: false,
      loopTop: false,
      loopBottom: false,
      loopHorizontal: false,
      slidesNavigation: true,
    }, _options);

    function init() {

      $container.each(function() {
        var $this = $(this);
        var $slides = $this.find('.slide');
        var slidesLength = $slides.length;
        slideIndex.push(0);
        slidesAmount.push($slides.length);
        $slides.width(100 / slidesLength + '%');
        $this.width(100 * slidesLength + '%');
      });

      $content.css({
        '-webkit-transition': 'transform ' + options.scrollingSpeed + 'ms ease',
        '-moz-transition': 'transform ' + options.scrollingSpeed + 'ms ease',
        'transition': 'transform ' + options.scrollingSpeed + 'ms ease'
      });

      $container.css({
        '-webkit-transition': 'transform ' + options.slidingSpeed + 'ms ease',
        '-moz-transition': 'transform ' + options.slidingSpeed + 'ms ease',
        'transition': 'transform ' + options.slidingSpeed + 'ms ease'
      });

      if (options.navigation) {
        $slider.append('<nav id="slider-nav"><ul></ul></nav>');
        $container.each(function() {
          $('#slider-nav ul').append('<li class="circle"></li>');
        });
        $('#slider-nav li').first().addClass('active');
      }

      if (options.slidesNavigation) {
        $('.slider-section').each(function() {
          var $section = $(this);

          $section.prepend('<div class="slides-nav"><ul></ul></div>');
          $section.find('.slide').each(function() {
            if (options.thumbnails && !isTouchDevice) {
              var imageSrc = $(this).css('background-image').slice(5, -2);
              $section.find('.slides-nav ul').append('<li class="thumbnail"><img src="' + imageSrc + '"></li>');
            } else {
              $section.find('.slides-nav ul').append('<li class="circle"></li>');
            }
          });
          $section.find('.slides-nav li').first().addClass('active');
        });
      }
    }

    init();

    var FullPageSlider = function() {
      var i = 0; // i - index of current section
      var canScroll = true;
      var touchStartX = 0;
      var touchStartY = 0;
      var touchEndX = 0;
      var touchEndY = 0;
      var self = this;

      this.moveRight = function() {
        if (canScroll) {
          if (slideIndex[i] < slidesAmount[i] - 1) {
            slideIndex[i]++;
          } else if (options.loopHorizontal) {
            slideIndex[i] = 0;
          } else {
            return;
          }
          setScroll(options.slidingSpeed);
          moveSlide();
        }
      }

      this.moveLeft = function() {
        if (canScroll) {
          if (slideIndex[i] > 0) {
            slideIndex[i]--;
          } else if (options.loopHorizontal) {
            slideIndex[i] = slidesAmount[i] - 1;
          } else {
            return;
          }
          setScroll(options.slidingSpeed);
          moveSlide();
        }
      }

      this.moveUp = function() {
        if (canScroll) {
          if (i > 0) {
            i--;
          } else if (options.loopTop) {
            i = $container.length - 1;
          } else {
            return;
          }
          setScroll(options.scrollingSpeed);
          moveSection();
          onLeave();
          afterSectionLoad();
        }
      }

      this.moveDown = function() {
        if (canScroll) {
          if (i < $container.length - 1) {
            i++;
          } else if (options.loopBottom) {
            i = 0;
          } else {
            return;
          }
          setScroll(options.scrollingSpeed);
          moveSection();
          onLeave();
          afterSectionLoad();
        }
      }

      function onLeave() {
        if (typeof options.onLeave === 'function') {
          options.onLeave(i);
        }
      }

      function afterSectionLoad() {
        if (typeof options.afterSectionLoad === 'function') {
          setTimeout(function() {
            options.afterSectionLoad(i);
          }, options.scrollingSpeed);
        }
      }

      function setScroll(speed) {
        canScroll = false;
        setTimeout(function() {
          canScroll = true;
        }, speed);
      }

      function moveSection() {
        $('#slider-nav li').removeClass('active').eq(i).addClass('active');
        setTranslate($content, 'translate3d', '0, ' + (-i * 100) + '%, 0');
      }

      function moveSlide() {
        $('.slides-nav').eq(i).find('li').removeClass('active').eq(slideIndex[i]).addClass('active');
        setTranslate($container.eq(i), 'translate3d', -slideIndex[i] * (100 / slidesAmount[i]) + '%, 0, 0');
      }

      function setTranslate($element, property, value) {
        $element.css({
          '-webkit-transform': property + '('+value+')',
          '-moz-transform': property + '('+value+')',
          '-ms-transform': property + '('+value+')',
          '-o-transform': property + '('+value+')',
          'transform': property + '('+value+')'
        });
      }

      function addEventListeners() {
        $('.slides-nav').on('click', 'li', function() {
          slideIndex[i] = $(this).index();
          moveSlide();
        });

        $('#slider-nav').on('click', 'li', function() {
          i = $(this).index();
          moveSection();
        });

        $(document).on('keydown', keyHandler);
        $(document).on('wheel', mousewheelHandler);

        $slider.on('touchstart', function(e) {
          touchStartX = e.originalEvent.changedTouches[0].pageX;
          touchStartY = e.originalEvent.changedTouches[0].pageY;
        });

        $slider.on('touchend', function(e) {
          touchEndX = e.originalEvent.changedTouches[0].pageX;
          touchEndY = e.originalEvent.changedTouches[0].pageY;
          touchHandler();
        });
      }

      addEventListeners();

      function touchHandler() {
        if (Math.abs(touchStartX - touchEndX) > Math.abs(touchStartY - touchEndY)) {
          if (touchStartX > touchEndX) {
            self.moveRight();
          } else if (touchStartX < touchEndX) {
            self.moveLeft();
          } else return;
        } else {
          if (touchStartY > touchEndY) {
            self.moveDown();
          } else if (touchStartY < touchEndY) {
            self.moveUp();
          } else return;
        }
      }

      function keyHandler(e) {

        switch (e.which) {
          case 33:
          case 38: // up arrow/page up
            self.moveUp();
          break;

          case 40:
          case 34: // down arrow/page down
            self.moveDown();
          break;

          case 37: // left arrow
            self.moveLeft();
          break;

          case 39: // right arrow
            self.moveRight();
          break;

          default:
          return;
        }
      }

      function mousewheelHandler(e) {
        e.originalEvent.deltaY > 0 ? self.moveDown() : self.moveUp();
      }
    };

    new FullPageSlider();

    if (typeof options.afterLoad === 'function') {
      options.afterLoad();
    }
  }
})(jQuery);